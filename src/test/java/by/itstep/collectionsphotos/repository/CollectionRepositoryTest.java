package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionCreateDto;
import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionFullDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserCreateDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserFullDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.interfaces.CollectionRepository;
import by.itstep.collectionsphotos.repository.interfaces.CommentRepository;
import by.itstep.collectionsphotos.repository.interfaces.PhotoRepository;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import by.itstep.collectionsphotos.service.CollectionService;
import by.itstep.collectionsphotos.util.GenerateUtil;
import by.itstep.collectionsphotos.utils.DataBaseCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CollectionRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private CollectionService collectionService;

    @Autowired
    private DataBaseCleaner dataBaseCleaner;

    @BeforeEach
    private void setUp() {
        dataBaseCleaner.clean();
    }

    @AfterEach
    private void shutDown() {
        dataBaseCleaner.clean();
    }

    @Test
    public void create_happyPath() {
        //given
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        CollectionEntity collection = GenerateUtil.prepareCollection(user);

        //when
        CollectionEntity createCollection = collectionRepository.create(collection);
        //then
        Assertions.assertNotNull(createCollection.getId());
        CollectionEntity foundCollection = collectionRepository.findById(createCollection.getId());

        Assertions.assertEquals(collection.getName(), foundCollection.getName());
        Assertions.assertEquals(collection.getDescription(), foundCollection.getDescription());
        Assertions.assertEquals(collection.getUser().getId(), foundCollection.getUser().getId());
    }

    @Test
    public void findById_happyPath() {
        //given
        CollectionEntity collection = addCollectionToDB();
        //when
        CollectionEntity found = collectionRepository.findById(collection.getId());
        //then
        Assertions.assertEquals(collection.getId(), found.getId());
    }

    @Test
    public void findAll_whenNoOneFound() {
        //given

        //when
        List<CollectionEntity> foundCollections = collectionRepository.findAll();
        //then
        Assertions.assertEquals(0, foundCollections.size());

        Assertions.assertTrue(foundCollections.isEmpty());
    }

    @Test
    public void findAll_happyPath() {
        //given
        addCollectionToDB();
        //when
        List<CollectionEntity> foundCollections = collectionRepository.findAll();
        //then
        Assertions.assertEquals(1, foundCollections.size());
    }

    @Test
    public void upDate_happyPath() {
        //given
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        CollectionEntity existCollection = addCollectionToDB();
        existCollection.setName("update");
        existCollection.setDescription("update");
        existCollection.setUser(user);

        //when
        CollectionEntity upDateCollection = collectionRepository.upDate(existCollection);
        //then
        Assertions.assertEquals(existCollection.getId(), upDateCollection.getId());
        collectionRepository.findById(existCollection.getId());

        Assertions.assertEquals(existCollection.getName(), upDateCollection.getName());
        Assertions.assertEquals(existCollection.getDescription(), upDateCollection.getDescription());
        Assertions.assertEquals(existCollection.getUser().getId(), upDateCollection.getUser().getId());
    }

    @Test
    public void delete_happyPath() {
        //given
        CollectionEntity existCollection = addCollectionToDB();
        //when
        collectionRepository.delete(existCollection.getId());
        //then
        CollectionEntity foundCollection = collectionRepository.findById(existCollection.getId());
        Assertions.assertNull(foundCollection);
    }

    private CollectionEntity addCollectionToDB() {
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        CollectionEntity collectionToDB = GenerateUtil.prepareCollection(user);
        return collectionRepository.create(collectionToDB);
    }



}
