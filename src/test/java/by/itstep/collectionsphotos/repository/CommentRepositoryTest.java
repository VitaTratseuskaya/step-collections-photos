package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.interfaces.CommentRepository;
import by.itstep.collectionsphotos.repository.interfaces.PhotoRepository;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import by.itstep.collectionsphotos.util.GenerateUtil;
import by.itstep.collectionsphotos.utils.DataBaseCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CommentRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private DataBaseCleaner dataBaseCleaner;

    @BeforeEach
    private void setUp() {
        dataBaseCleaner.clean();
    }

    @AfterEach
    private void shutDown() {
        dataBaseCleaner.clean();
    }

    @Test
    public void create_happyPath() {
        //given
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        PhotoEntity photo = GenerateUtil.preparePhoto();
        photoRepository.create(photo);

        CommentEntity commentToSave = GenerateUtil.prepareComment(user, photo);
        //when
        CommentEntity savedComment = commentRepository.create(commentToSave);
        //then
        Assertions.assertNotNull(savedComment.getId());
        CommentEntity foundComment = commentRepository.findById(savedComment.getId());

        Assertions.assertEquals(commentToSave.getMessage(), foundComment.getMessage());
        Assertions.assertEquals(commentToSave.getUser().getId(), foundComment.getUser().getId());
        Assertions.assertEquals(commentToSave.getPhotos().getId(), foundComment.getPhotos().getId());

    }

    @Test
    public void findAll_happyPath() {
        //given
        addCommentToDB();
        //when
        List<CommentEntity> found = commentRepository.findAll();
        //then
        Assertions.assertEquals(1, found.size());
    }

    @Test
    public void findAll_whenNoOneFound() {
        //given

        //when
        List<CommentEntity> found = commentRepository.findAll();
        //then
        Assertions.assertEquals(0, found.size());
        Assertions.assertTrue(found.isEmpty());
    }

    @Test
    public void findById_happyPath() {
        //given
        CommentEntity exist = addCommentToDB();
        //when
        CommentEntity found = commentRepository.findById(exist.getId());
        //then
        Assertions.assertEquals(exist.getId(), found.getId());
    }

    @Test
    public void upDate_happyPath(){
        //given
        CommentEntity existComment = addCommentToDB();

        UserEntity existUser = GenerateUtil.prepareUser();
        userRepository.create(existUser);
        PhotoEntity existPhoto = GenerateUtil.preparePhoto();
        photoRepository.create(existPhoto);

        existComment.setMessage("update");
        existComment.setUser(existUser);
        existComment.setPhotos(existPhoto);
        //when
        CommentEntity updateComment = commentRepository.upDate(existComment);
        //then
        Assertions.assertEquals(existComment.getId(), updateComment.getId());
        commentRepository.findById(existComment.getId());

        Assertions.assertEquals(existComment.getMessage(), updateComment.getMessage());
        Assertions.assertEquals(existComment.getUser().getId(), updateComment.getUser().getId());
        Assertions.assertEquals(existComment.getPhotos().getId(), updateComment.getPhotos().getId());

    }

    @Test
    public void delete_happyPath(){
        //given
        CommentEntity existComment = addCommentToDB();
        //when
        commentRepository.delete(existComment.getId());
        //then
        CommentEntity foundComment = commentRepository.findById(existComment.getId());
        Assertions.assertNull(foundComment);
    }

    private CommentEntity addCommentToDB() {
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        PhotoEntity photo = GenerateUtil.preparePhoto();
        photoRepository.create(photo);

        CommentEntity commentToDB = GenerateUtil.prepareComment(user, photo);
        return commentRepository.create(commentToDB);
    }
}

