package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.repository.interfaces.CommentRepository;
import by.itstep.collectionsphotos.repository.interfaces.PhotoRepository;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import by.itstep.collectionsphotos.util.GenerateUtil;
import by.itstep.collectionsphotos.utils.DataBaseCleaner;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Random;

@SpringBootTest
public class PhotoRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private DataBaseCleaner dataBaseCleaner;

    @BeforeEach
    private void setUp() {
        dataBaseCleaner.clean();
    }

    @AfterEach
    private void shutDown() {
        dataBaseCleaner.clean();
    }

    @Test
    public void create_happyPath() {
        //given
        PhotoEntity photo = GenerateUtil.preparePhoto();
        //when
        PhotoEntity createdPhoto = photoRepository.create(photo);
        //then

        Assertions.assertNotNull(createdPhoto.getId());
        PhotoEntity foundPhoto = photoRepository.findById(createdPhoto.getId());

        Assertions.assertEquals(photo.getName(), foundPhoto.getName());
        Assertions.assertEquals(photo.getLink(), foundPhoto.getLink());
        Assertions.assertEquals(photo.getRating(), foundPhoto.getRating());

    }

    @Test
    public void findById() {
        //given
        PhotoEntity photo = addPhotoToDB();
        //when
        PhotoEntity found = photoRepository.findById(photo.getId());
        //then
       Assertions.assertEquals(photo.getId(), found.getId());
    }

    @Test
    public void findAll_happyPath(){
        //given
        addPhotoToDB();
        //when
        List<PhotoEntity> foundPhoto = photoRepository.findAll();
        //then
        Assertions.assertEquals(1, foundPhoto.size());
    }

    @Test
    public void findAll_whenNoOneFound() {
        //given

        //when
        List<PhotoEntity> foundPhoto = photoRepository.findAll();
        //then
        Assertions.assertEquals(0, foundPhoto.size());
        Assertions.assertTrue(foundPhoto.isEmpty());

    }

    @Test
    public void upDate_happyPath(){
        //given
        Faker faker = new Faker(new Random());
        PhotoEntity exist = addPhotoToDB();

        exist.setName("update");
        exist.setLink("update");
        exist.setRating(faker.number().randomDigit());
        //when
        PhotoEntity upDate = photoRepository.upDate(exist);
        //then
        Assertions.assertEquals(exist.getId(), upDate.getId());
        photoRepository.findById(exist.getId());

        Assertions.assertEquals(exist.getName(), upDate.getName());
        Assertions.assertEquals(exist.getLink(), upDate.getLink());
        Assertions.assertEquals(exist.getRating(), upDate.getRating());

    }

    @Test
    public void delete_happyPath(){
        //given
        PhotoEntity existPhoto = addPhotoToDB();
        //when
        photoRepository.delete(existPhoto.getId());
        //then
        PhotoEntity foundPhoto = photoRepository.findById(existPhoto.getId());
        Assertions.assertNull(foundPhoto);
    }

    private PhotoEntity addPhotoToDB() {
        PhotoEntity photoEntityToDB = GenerateUtil.preparePhoto();
        return photoRepository.create(photoEntityToDB);
    }

}
