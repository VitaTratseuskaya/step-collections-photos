package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import by.itstep.collectionsphotos.util.GenerateUtil;
import by.itstep.collectionsphotos.utils.DataBaseCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DataBaseCleaner dataBaseCleaner;

    @BeforeEach
    private void setUp() {
        dataBaseCleaner.clean();
    }

    @AfterEach
    private void shutDown() {
        dataBaseCleaner.clean();
    }

    @Test
    public void create_happyPath() {
        //given
        UserEntity user = GenerateUtil.prepareUser();

        //when
        UserEntity createdUser = userRepository.create(user);

        //then
        Assertions.assertNotNull(createdUser.getId());
        UserEntity foundUser = userRepository.findById(createdUser.getId());

        Assertions.assertEquals(user.getName(), foundUser.getName());
        Assertions.assertEquals(user.getPassword(), foundUser.getPassword());
        Assertions.assertEquals(user.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(user.getLogin(), foundUser.getLogin());

    }

    @Test
    public void findById_happyPath() {
        //given
        UserEntity user = addUserToDataBase();
        //when
        UserEntity found = userRepository.findById(user.getId());
        //then
        Assertions.assertEquals(user.getId(), found.getId());
    }

    @Test
    public void findAll_whenNoOneFound() {
        //given

        //when
        List<UserEntity> foundUsers = userRepository.findAll();

        //then
        Assertions.assertEquals(0, foundUsers.size());

        Assertions.assertTrue(foundUsers.isEmpty());
    }

    @Test
    public void findAll_happyPath() {
        //given
        addUserToDataBase();

        //when
        List<UserEntity> foundUsers = userRepository.findAll();

        //then
        Assertions.assertEquals(1, foundUsers.size());
    }

    @Test
    public void upDate_happyPath() {
        //given
        UserEntity existingUser = addUserToDataBase();
        existingUser.setEmail("update");
        existingUser.setLogin("update");
        existingUser.setPassword("update");

        //when
        UserEntity upDatedUser = userRepository.upDate(existingUser);

        //then
        Assertions.assertEquals(existingUser.getId(), upDatedUser.getId());
        userRepository.findById(existingUser.getId());

        Assertions.assertEquals(existingUser.getName(), upDatedUser.getName());
        Assertions.assertEquals(existingUser.getPassword(), upDatedUser.getPassword());
        Assertions.assertEquals(existingUser.getLogin(), upDatedUser.getLogin());
        Assertions.assertEquals(existingUser.getEmail(), upDatedUser.getEmail());
    }

    @Test
    public void delete_HappyPath() {
        //given
        UserEntity existUser = addUserToDataBase();
        //when
        userRepository.delete(existUser.getId());
        //then
        UserEntity foundUser = userRepository.findById(existUser.getId());
        Assertions.assertNull(foundUser);
    }

    private UserEntity addUserToDataBase() {
        UserEntity userToAdd = GenerateUtil.prepareUser();
        return userRepository.create(userToAdd);
    }


}
