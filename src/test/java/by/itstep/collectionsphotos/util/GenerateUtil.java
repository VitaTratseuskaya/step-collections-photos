package by.itstep.collectionsphotos.util;

import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionCreateDto;
import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionUpdateDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoCreateDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoUpdateDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserCreateDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserFullDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserUpdateDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentCreateDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.interfaces.CollectionRepository;
import by.itstep.collectionsphotos.repository.interfaces.CommentRepository;
import by.itstep.collectionsphotos.repository.interfaces.PhotoRepository;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import com.github.javafaker.Faker;

import java.util.Random;

public class GenerateUtil {

    private static UserRepository userRepository;
    private static PhotoRepository photoRepository;
    private static CommentRepository commentRepository;
    private static CollectionRepository collectionRepository;

    private static Faker faker;

    public static UserEntity prepareUser() {
        Faker faker = new Faker(new Random());
        UserEntity user = new UserEntity();
        user.setName(faker.name().firstName());
        user.setPassword(faker.superhero().name());
        user.setEmail(faker.name().username());
        user.setLogin(faker.pokemon().name());

        return user;
    }

    public static UserCreateDto prepareUserCreateDto() {
        UserCreateDto user = new UserCreateDto();
        user.setName(faker.name().firstName());
        user.setPassword(faker.superhero().name());
        user.setEmail(faker.company().logo());
        user.setLogin(faker.pokemon().name());

        return user;
    }

    public static UserUpdateDto prepareUserUpdateDto() {
        Faker faker = new Faker(new Random());
        UserUpdateDto user = new UserUpdateDto();
        user.setName(faker.name().firstName());
        user.setLogin(faker.pokemon().name());

        return user;
    }

    public static PhotoEntity preparePhoto() {
        PhotoEntity photo = new PhotoEntity();
        Faker faker = new Faker(new Random());
        photo.setLink(faker.name().nameWithMiddle());
        photo.setName(faker.name().nameWithMiddle());
        photo.setRating(5);

        return photo;
    }

    public static PhotoCreateDto preparePhotoCreateDto() {
        PhotoCreateDto photo = new PhotoCreateDto();
        photo.setName(faker.superhero().descriptor());
        photo.setLink(faker.name().nameWithMiddle());

        return photo;
    }

    public static PhotoUpdateDto preparePhotoUpdateDto() {
        Faker faker = new Faker(new Random());
        PhotoUpdateDto photo = new PhotoUpdateDto();
        photo.setName(faker.name().firstName());
        return photo;
    }

    public static CommentEntity prepareComment(UserEntity user, PhotoEntity photo) {
        Faker faker = new Faker(new Random());
        CommentEntity comment = new CommentEntity();
        comment.setMessage(faker.name().title());
        comment.setUser(user);
        comment.setPhotos(photo);
        return comment;
    }

    public static CommentCreateDto prepareCommentCreateDto(UserEntity user, PhotoEntity photo) {
        CommentCreateDto comment = new CommentCreateDto();
        comment.setMessage(faker.expression("ololo"));
        return comment;
    }

    public static CommentUpdateDto prepareCommentUpdateDto(UserEntity user, PhotoEntity photo) {
        CommentUpdateDto comment = new CommentUpdateDto();
        comment.setMessage(faker.company().catchPhrase());
        return comment;
    }

    public static CollectionEntity prepareCollection(UserEntity user) {
        CollectionEntity collection = new CollectionEntity();
        collection.setName(faker.name().name());
        collection.setDescription(faker.name().title());
        collection.setUser(user);

        return collection;
    }

    public static CollectionCreateDto prepareCollectionCreateDto(UserEntity user) {
        CollectionCreateDto collection = new CollectionCreateDto();
        collection.setName(faker.name().name());
        collection.setDescription(faker.name().title());
        return collection;
    }

    public static CollectionUpdateDto prepareCollectionUpdateDto(UserEntity user) {
        CollectionUpdateDto collection = new CollectionUpdateDto();
        collection.setName(faker.superhero().prefix());
        collection.setDescription(faker.company().profession());
        return collection;
    }

}
