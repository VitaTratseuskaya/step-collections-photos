package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionCreateDto;
import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionFullDto;
import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionShortDto;
import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CollectionMapper;
import by.itstep.collectionsphotos.repository.interfaces.CollectionRepository;
import by.itstep.collectionsphotos.repository.interfaces.CommentRepository;
import by.itstep.collectionsphotos.repository.interfaces.PhotoRepository;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import by.itstep.collectionsphotos.util.GenerateUtil;
import by.itstep.collectionsphotos.utils.DataBaseCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CollectionServiceTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CollectionService collectionService;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private CollectionMapper collectionMapper;

    @Autowired
    private DataBaseCleaner dataBaseCleaner;

    @BeforeEach
    private void setUp() {
        dataBaseCleaner.clean();
    }

    @AfterEach
    private void shutDown() {
        dataBaseCleaner.clean();
    }

  /*  @Test
    public void create_happyPath() {
        //given
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);
        CollectionCreateDto collection = GenerateUtil.prepareCollectionCreateDto(user);
        //when
        CollectionFullDto createdCollection = collectionService.create(collection, user.getId());
        //then
        Assertions.assertNotNull(createdCollection.getId());
    }*/

    @Test
    public void findById_happyPath() {
        //given
        CollectionEntity collection = addCollectionToDB();
        //when
        CollectionFullDto found = collectionService.findById(collection.getId());
        //then
        Assertions.assertEquals(collection.getId(), found.getId());
    }

    @Test
    public void findById_IdNull() {
        //given
        boolean collectionNull = false;
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);
        CollectionEntity collection = GenerateUtil.prepareCollection(user);
        collection.setId(0);

        //when
        try {
            collectionService.findById(collection.getId());
        } catch (RuntimeException ex) {
            collectionNull = true;
        }

        //then
        Assertions.assertTrue(collectionNull);
    }

    @Test
    public void findAll_isEmpty() {
        //given

        //when
        List<CollectionShortDto> allCollection = collectionService.findAll();
        //then
        Assertions.assertTrue(allCollection.isEmpty());
    }

    @Test
    public void findAll_happyPath() {
        //given
        addCollectionToDB();
        //when
        List<CollectionShortDto> allCollection = collectionService.findAll();
        //then
        Assertions.assertEquals(1, allCollection.size());
    }

   /* @Test
    public void upDate_withoutId() {
        //given
        boolean upDateNull = false;
        UserEntity user = GenerateUtil.prepareUser();
        CollectionUpdateDto collection = GenerateUtil.prepareCollectionUpdateDto(user);
        collection.setId(0);

        //when
        try {
            collectionService.upDate(collection, user.getId());
        } catch (RuntimeException ex) {
            upDateNull = true;
        }

        //then
        Assertions.assertTrue(upDateNull);
    }*/

   /* @Test
    public void upDate_happyPath() {
        //given
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        CollectionFullDto existingCollection = addCollectionDtoToDataBase();
        existingCollection.setName("update");
        existingCollection.setDescription("update");

        CollectionUpdateDto collection = collectionMapper.mapFullToUpdate(existingCollection);

        //when
        CollectionFullDto upDatedCollection = collectionService.upDate(collection, user.getId());

        //then
        Assertions.assertEquals(existingCollection.getId(), upDatedCollection.getId());
        userRepository.findById(existingCollection.getId());

        Assertions.assertEquals(existingCollection.getName(), upDatedCollection.getName());
        Assertions.assertEquals(existingCollection.getDescription(), upDatedCollection.getDescription());
    }*/

    @Test
    public void delete_happyPath() {
        //given
        CollectionEntity existCollection = addCollectionToDB();
        //when
        collectionService.delete(existCollection.getId());
        //then
        CollectionEntity foundCollection = collectionRepository.findById(existCollection.getId());
        Assertions.assertNull(foundCollection);
    }

    @Test
    public void delete_withoutId() {
        //given
        boolean deleteNull = false;

        CollectionEntity existCollection = addCollectionToDB();
        existCollection.setId(0);

        //when
        try {
            collectionService.delete(existCollection.getId());
        } catch (RuntimeException ex) {
            deleteNull = true;
        }

        //then
        Assertions.assertTrue(deleteNull);
    }

    private CollectionEntity addCollectionToDB() {
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        CollectionEntity collectionToDB = GenerateUtil.prepareCollection(user);
        return collectionRepository.create(collectionToDB);
    }

    private CollectionFullDto addCollectionDtoToDataBase() {
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        CollectionCreateDto createDto = GenerateUtil.prepareCollectionCreateDto(user);

        return collectionService.create(createDto);
    }
}
