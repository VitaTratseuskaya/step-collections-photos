package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.UserDTO.UserCreateDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserFullDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentCreateDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentFullDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentShortDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentUpdateDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CommentMapper;
import by.itstep.collectionsphotos.repository.interfaces.CommentRepository;
import by.itstep.collectionsphotos.repository.interfaces.PhotoRepository;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import by.itstep.collectionsphotos.util.GenerateUtil;
import by.itstep.collectionsphotos.utils.DataBaseCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CommentServiceTest {

    @Autowired
    private CommentService commentService;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;

    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private DataBaseCleaner cleaner;

    @BeforeEach
    public void setUp() {
        cleaner.clean();
    }

    @AfterEach
    private void shutDown() {
        cleaner.clean();
    }

    @Test
    public void create_happyPath() {
        //given
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        PhotoEntity photo = GenerateUtil.preparePhoto();
        photoRepository.create(photo);

        CommentCreateDto comment = GenerateUtil.prepareCommentCreateDto(user, photo);

        //when
        CommentFullDto createdComment = commentService.create(comment);
        //then
        Assertions.assertNotNull(createdComment.getId());
    }

    @Test
    public void findById_happyPath() {
        //given
        CommentEntity comment = addCommentToDB();
        //when
        CommentFullDto found = commentService.findById(comment.getId());
        //then
        Assertions.assertEquals(comment.getId(), found.getId());
    }

    @Test
    public void findById_IdNull() {
        //given
        boolean commentNull = false;
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        PhotoEntity photo = GenerateUtil.preparePhoto();
        photoRepository.create(photo);

        CommentEntity comment = GenerateUtil.prepareComment(user, photo);
        comment.setId(0);

        //when
        try {
            commentService.findById(comment.getId());
        } catch (RuntimeException ex) {
            commentNull = true;
        }

        //then
        Assertions.assertTrue(commentNull);
    }

    @Test
    public void findAll_happyPath() {
        //given
        addCommentToDB();
        //when
        List<CommentShortDto> allComments = commentService.findAll();
        //then
        Assertions.assertEquals(1, allComments.size());
    }

    @Test
    public void findAll_isEmpty() {
        //given

        //when
        List<CommentShortDto> allComments = commentService.findAll();
        //then
        Assertions.assertTrue(allComments.isEmpty());
    }

    @Test
    public void upDate_withoutId() {
        //given
        boolean upDateNull = false;
        UserEntity user = GenerateUtil.prepareUser();
        PhotoEntity photo = GenerateUtil.preparePhoto();
        CommentUpdateDto comment = GenerateUtil.prepareCommentUpdateDto(user, photo);
        comment.setId(0);

        //when
        try {
            commentService.upDate(comment);
        } catch (RuntimeException ex) {
            upDateNull = true;
        }

        //then
        Assertions.assertTrue(upDateNull);
    }

  /*  @Test
    public void upDate_happyPath() {
        //given
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        PhotoEntity photo = GenerateUtil.preparePhoto();
        photoRepository.create(photo);

        CommentFullDto existingComment = addCommentDtoToDataBase();
       // existingComment.setMessage("update");
        CommentUpdateDto comment = commentMapper.mapFullToUpdateDto(existingComment);

        //when
        CommentFullDto upDated = commentService.upDate(comment);

        //then
        Assertions.assertEquals(existingComment.getId(), upDated.getId());
        userRepository.findById(existingComment.getId());

        Assertions.assertEquals(existingComment.getMessage(), upDated.getMessage());
    }*/

    @Test
    public void delete_happyPath() {
        //given
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        PhotoEntity photo = GenerateUtil.preparePhoto();
        photoRepository.create(photo);

        CommentEntity comment = addCommentToDB();

        //when
        commentService.delete(comment.getId());

        //then
        Assertions.assertNotNull(userRepository.findById(user.getId()));
        Assertions.assertNotNull(photoRepository.findById(photo.getId()));
        Assertions.assertNull(commentRepository.findById(comment.getId()));
    }

    private CommentEntity addCommentToDB() {
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        PhotoEntity photo = GenerateUtil.preparePhoto();
        photoRepository.create(photo);

        CommentEntity commentToDB = GenerateUtil.prepareComment(user, photo);
        return commentRepository.create(commentToDB);
    }

    private CommentFullDto addCommentDtoToDataBase() {
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        PhotoEntity photo = GenerateUtil.preparePhoto();
        photoRepository.create(photo);

        CommentCreateDto createDto = GenerateUtil.prepareCommentCreateDto(user, photo);
        return commentService.create(createDto);
    }

}
