package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.UserDTO.UserCreateDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserFullDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserShortDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserUpdateDto;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.UserMapper;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import by.itstep.collectionsphotos.util.GenerateUtil;
import by.itstep.collectionsphotos.utils.DataBaseCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserServiceTest {
    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private DataBaseCleaner cleaner;


    @BeforeEach
    public void setUp() {
        cleaner.clean();
    }

    @AfterEach
    private void shutDown() {
        cleaner.clean();
    }

    @Test
    public void create_happyPath() {
        //given
        UserCreateDto user = GenerateUtil.prepareUserCreateDto();
        //when
        UserFullDto createdUser = userService.create(user);
        //then
        Assertions.assertNotNull(createdUser.getId());
    }

    @Test
    public void findById_happyPath() {
        //given
        UserEntity user = addUserToDataBase();
        //when
        UserFullDto found = userService.findById(user.getId());
        //then
        Assertions.assertEquals(user.getId(), found.getId());
    }

    @Test
    public void findById_IdNull() {
        //gvien
        boolean userNull = false;
        UserEntity user = GenerateUtil.prepareUser();
        user.setId(0);

        //when
        try {
            userService.findById(user.getId());
        } catch (RuntimeException ex) {
            userNull = true;
        }

        //then
        Assertions.assertTrue(userNull);
    }

    @Test
    public void findAll_isEmpty() {
        //given

        //when
        List<UserShortDto> allUser = userService.findAll();
        //then
        Assertions.assertTrue(allUser.isEmpty());
    }

    @Test
    public void findAll_happyPath() {
        //given
        addUserToDataBase();
        //when
        List<UserShortDto> allUser = userService.findAll();
        //then
        Assertions.assertEquals(1, allUser.size());
    }

    @Test
    public void upDate_withoutId() {
        //given
        boolean upDateNull = false;
        UserUpdateDto user = GenerateUtil.prepareUserUpdateDto();
        user.setId(0);

        //when
        try {
            userService.upDate(user);
        } catch (RuntimeException ex) {
            upDateNull = true;
        }

        //then
        Assertions.assertTrue(upDateNull);
    }

    @Test
    public void upDate_happyPath() {
        //given
        UserFullDto existingUser = addUserDtoToDataBase();
        UserUpdateDto userUpdate = new UserUpdateDto();
        existingUser.setLogin("update");
        existingUser.setName("update");

        //when
        UserFullDto upDatedUser = userService.upDate(userUpdate);

        //then
        Assertions.assertEquals(existingUser.getId(), upDatedUser.getId());
        UserEntity userAfterUpdate = userRepository.findById(existingUser.getId());

        Assertions.assertEquals(userAfterUpdate.getLogin(), upDatedUser.getLogin());
        Assertions.assertEquals(userAfterUpdate.getName(), userUpdate.getName());
    }

    @Test
    public void delete_happyPath() {
        //given
        UserEntity existUser = addUserToDataBase();
        //when
        userService.delete(existUser.getId());
        //then
        UserEntity foundUser = userRepository.findById(existUser.getId());
        Assertions.assertNull(foundUser);
    }

    @Test
    public void delete_withoutId() {
        //given
        boolean deleteNull = false;

        UserEntity existUser = addUserToDataBase();
        existUser.setId(0);

        //when
        try {
            userService.delete(existUser.getId());
        } catch (RuntimeException ex) {
            deleteNull = true;
        }

        //then
        Assertions.assertTrue(deleteNull);
    }

    private UserEntity addUserToDataBase() {
        UserEntity userToAdd = GenerateUtil.prepareUser();
        return userRepository.create(userToAdd);
    }

    private UserFullDto addUserDtoToDataBase() {
        UserCreateDto createDto = GenerateUtil.prepareUserCreateDto();
        return userService.create(createDto);
    }

}
