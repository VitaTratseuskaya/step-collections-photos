package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoCreateDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoFullDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoShortDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoUpdateDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserCreateDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserFullDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.PhotoMapper;
import by.itstep.collectionsphotos.repository.*;
import by.itstep.collectionsphotos.repository.interfaces.CollectionRepository;
import by.itstep.collectionsphotos.repository.interfaces.PhotoRepository;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import by.itstep.collectionsphotos.util.GenerateUtil;
import by.itstep.collectionsphotos.utils.DataBaseCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class PhotoServiceTest {

    @Autowired
    private PhotoService photoService;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CollectionRepository collectionRepository;

    @Autowired
    private PhotoMapper photoMapper;
    @Autowired
    private DataBaseCleaner dataBaseCleaner;

    @BeforeEach
    public void setUp() {
        dataBaseCleaner.clean();
    }

    @AfterEach
    private void shutDown() {
        dataBaseCleaner.clean();
    }

    @Test
    public void create_happyPath() {
        //given
        PhotoCreateDto photo = GenerateUtil.preparePhotoCreateDto();
        //when
        PhotoFullDto createdPhoto = photoService.create(photo);
        //then
        Assertions.assertNotNull(createdPhoto.getId());
    }

    @Test
    public void findById_happyPath() {
        //given
        PhotoEntity photo = addPhotoToDB();
        //when
        PhotoEntity found = photoService.findById(photo.getId());
        //then
        Assertions.assertEquals(photo.getId(), found.getId());
    }

    @Test
    public void findById_IdNull() {
        //given
        boolean photoNull = false;
        PhotoEntity photo = GenerateUtil.preparePhoto();
        photo.setId(0);

        //when
        try {
            photoService.findById(photo.getId());
        } catch (RuntimeException ex) {
            photoNull = true;
        }

        //then
        Assertions.assertTrue(photoNull);
    }

    @Test
    public void findAll_happyPath() {
        //given
        addPhotoToDB();
        //when
        List<PhotoShortDto> allPhotos = photoService.findAll();
        //then
        Assertions.assertEquals(1, allPhotos.size());
    }

    @Test
    public void findAll_isEmpty() {
        //given

        //when
        List<PhotoShortDto> allPhotos = photoService.findAll();
        //then
        Assertions.assertTrue(allPhotos.isEmpty());
    }

    @Test
    public void upDate_withoutId() {
        //given
        boolean upDateNull = false;
        PhotoUpdateDto photo = GenerateUtil.preparePhotoUpdateDto();
        photo.setId(0);

        //when
        try {
            photoService.upDate(photo);
        } catch (RuntimeException ex) {
            upDateNull = true;
        }

        //then
        Assertions.assertTrue(upDateNull);
    }

     @Test
   public void upDate_happyPath() {
        //given
        PhotoFullDto existingPhoto = addPhotoDtoToDataBase();
        PhotoUpdateDto updateRequest = new PhotoUpdateDto();
        updateRequest.setId(existingPhoto.getId());
        updateRequest.setName("test");

        //when
        PhotoFullDto upDated = photoService.upDate(updateRequest);

        //then
        Assertions.assertEquals(existingPhoto.getId(), upDated.getId());
        PhotoEntity entityAfterUpdate =  photoRepository.findById(existingPhoto.getId());

        Assertions.assertEquals(entityAfterUpdate.getName(), updateRequest.getName());
    }
    @Test
    public void delete_happyPath() {
        //given
        //1. создать и созранить в бд юзера
        //2. создать и сохрнанить в бд коллекцию
        //3. создать и сохранить в бд фотку
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        CollectionEntity collection = GenerateUtil.prepareCollection(user);
        collectionRepository.create(collection);

        PhotoEntity photo = GenerateUtil.preparePhoto();
        photoRepository.create(photo);


        collection.setPhotos(Arrays.asList(photo));
        collectionRepository.upDate(collection);

        //заполняем Many-To-Many
        collection.setPhotos(new ArrayList<>());
        collectionRepository.upDate(collection);

        //when
        //1. удаление одной фотки
        photoService.delete(photo.getId());

        //then
        //1. проверить что юзер не удалилась
        //2. проверить сто коллекция не удалилась
        //3. проверить что фотка удалилась
        Assertions.assertNotNull(userRepository.findById(user.getId()));
        Assertions.assertNotNull(collectionRepository.findById(collection.getId()));
        Assertions.assertNull(photoRepository.findById(photo.getId()));
    }

    private PhotoEntity addPhotoToDB() {
        PhotoEntity photoEntityToDB = GenerateUtil.preparePhoto();
        return photoRepository.create(photoEntityToDB);
    }

    private PhotoFullDto addPhotoDtoToDataBase() {
        PhotoCreateDto createDto = GenerateUtil.preparePhotoCreateDto();
        return photoService.create(createDto);
    }


}
