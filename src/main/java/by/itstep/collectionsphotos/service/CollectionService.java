package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionCreateDto;
import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionFullDto;
import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionShortDto;
import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CollectionMapStruct;
import by.itstep.collectionsphotos.mapper.CollectionMapper;
import by.itstep.collectionsphotos.repository.interfaces.CollectionRepository;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollectionService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CollectionRepository collectionRepository;


    public CollectionFullDto findById(int id) {
        CollectionEntity collection = collectionRepository.findById(id);

        if (collection == null) {
            throw new RuntimeException("Collection not found by id " + id);
        }
        System.out.println("CollectionService -> Found collection: " + collection.getId());

        return CollectionMapStruct.MAPPER.mapToFullDto(collection);
    }

    public List<CollectionShortDto> findAll() {
        List<CollectionEntity> allCollection = collectionRepository.findAll();
        System.out.println("CollectionService -> Found allCollections : " + allCollection);

        if (allCollection.isEmpty()) {
            System.out.println("Collection isEmpty");
        }

        return CollectionMapStruct.MAPPER.mapToListShortDto(allCollection);
    }

    public CollectionFullDto upDate(CollectionUpdateDto updateRequest) {
        CollectionEntity entityToUpdate = collectionRepository.findById(updateRequest.getId());
        if (updateRequest.getId() == null) {
            throw new RuntimeException("CollectionService -> " +
                    "Can't update entity without id ");
        }

        if (collectionRepository.findById(updateRequest.getId()) == null) {
            throw new RuntimeException("CollectionService -> Collection not found by id: " + updateRequest.getId());
        }

        UserEntity user = userRepository.findById(updateRequest.getId());

        if (user == null) {
            throw new RuntimeException("CollectionService - > User not found by id " + user.getId());
        }

        entityToUpdate.setName(updateRequest.getName());
        entityToUpdate.setDescription(updateRequest.getDescription());
        entityToUpdate.setUser(user);
        CollectionEntity updateCollection = collectionRepository.upDate(entityToUpdate);
        System.out.println("CollectionService -> Update collection: " + updateCollection);

        return CollectionMapStruct.MAPPER.mapToFullDto(updateCollection);
    }

    public CollectionFullDto create(CollectionCreateDto createRequest) {
        CollectionEntity collection = CollectionMapStruct.MAPPER.mapToEntity(createRequest);

        if (collection.getId() != null) {
            throw new RuntimeException("CollectionService -> Can't create entity which already has id " + collection.getId());
        }

        UserEntity user = userRepository.findById(collection.getUser().getId());
        collection.setUser(user);

        if (user == null) {
            throw new RuntimeException("CollectionService -> User not found by id " + collection.getId());
        }

        CollectionEntity createdCollection = collectionRepository.create(collection);
        System.out.println("CollectionService -> Created collection: " + createRequest);
        CollectionFullDto createdDto = CollectionMapStruct.MAPPER.mapToFullDto(createdCollection);
        return createdDto;
    }

    public void delete(int id) {
        CollectionEntity collectionForDelete = collectionRepository.findById(id);
        if (collectionForDelete == null) {
            throw new RuntimeException("CollectionService -> Collection not found " + collectionForDelete.getId());
        }

        List<CollectionEntity> allCollection = collectionRepository.findAll();
        for (CollectionEntity collection : allCollection) {
            collectionRepository.delete(collection.getId());
        }
    }

}