package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.commentDTO.CommentCreateDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentFullDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentShortDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentUpdateDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CommentMapStruct;
import by.itstep.collectionsphotos.mapper.CommentMapper;
import by.itstep.collectionsphotos.repository.interfaces.CommentRepository;
import by.itstep.collectionsphotos.repository.interfaces.PhotoRepository;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CommentRepository commentRepository;


    public CommentFullDto create(CommentCreateDto createdRequest) {
        CommentEntity comment = CommentMapStruct.MAPPER.mapToEntity(createdRequest);
        if (comment.getId() != null) {
            throw new RuntimeException("CommentService -> Can't create entity which already has id " + comment.getId());
        }

        UserEntity user = userRepository.findById(createdRequest.getUserId());
        comment.setUser(user);

        if (user == null) {
            throw new RuntimeException("CommentService -> User not found by id " + comment.getUser().getId());
        }

        PhotoEntity photo = photoRepository.findById(createdRequest.getPhotoId());
        comment.setPhotos(photo);

        if (photo == null) {
            throw new RuntimeException("CommentService -> User not found by id " + comment.getUser().getId());
        }

        comment.setPhotos(photoRepository.findById(createdRequest.getPhotoId()));
        comment.setUser(userRepository.findById(createdRequest.getUserId()));
        CommentEntity createdComment = commentRepository.create(comment);
        System.out.println("CommentService -> Created comment: " + createdRequest);

        return CommentMapStruct.MAPPER.mapToFullDto(createdComment);
    }

    public CommentFullDto findById(int id) {
        CommentEntity comment = commentRepository.findById(id);
        if (comment == null) {
            throw new RuntimeException("Comment not found by id " + id);
        }

        return CommentMapStruct.MAPPER.mapToFullDto(comment);
    }

    public List<CommentShortDto> findAll() {
        List<CommentEntity> allComment = commentRepository.findAll();
        System.out.println("Comment -> Found all comments: " + allComment);

        return CommentMapStruct.MAPPER.mapToListShortDto(allComment);
    }

    public CommentFullDto upDate(CommentUpdateDto updateRequest) {
        CommentEntity entityToUpdate = commentRepository.findById(updateRequest.getId());

        if(entityToUpdate == null) {
            throw new RuntimeException("Comment is not found by id " + entityToUpdate.getId());
        }
        if (updateRequest.getId() == null) {
            throw new RuntimeException("CommentService -> Can't update without id " + updateRequest.getId());
        }

        UserEntity user = userRepository.findById(updateRequest.getId());
        //  updateRequest.setUser(user);
        if (user == null) {
            throw new RuntimeException("CommentService -> User not found by id " + user.getId());
        }

        PhotoEntity photo = photoRepository.findById(updateRequest.getId());
        //   commentEntity.setPhotos(photo);
        if (photo == null) {
            throw new RuntimeException("CommentService -> User not found by id " + photo.getId());
        }

        entityToUpdate.setMessage(updateRequest.getMessage());

        CommentEntity updateComment = commentRepository.upDate(entityToUpdate);
        System.out.println("CommentService -> Found comment: " + updateRequest);// CommentFullDto updateDto = commentMapper.map(updateComment);

        return CommentMapStruct.MAPPER.mapToFullDto(updateComment);
    }

    public void delete(int id) {
        CommentEntity commentToDelete = commentRepository.findById(id);
        if (commentToDelete == null) {
            throw new RuntimeException("CommentService -> User was not found by id: " + id);
        }
        commentRepository.delete(id);
    }

}
