package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.Config.SecurityService;
import by.itstep.collectionsphotos.dto.UserDTO.UserCreateDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserShortDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserFullDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.UserMapStruct;
import by.itstep.collectionsphotos.repository.interfaces.CollectionRepository;
import by.itstep.collectionsphotos.repository.interfaces.CommentRepository;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private SecurityService securityService;


    public UserFullDto findById(int id) {
        UserEntity user = userRepository.findById(id);
        if (user == null) {
            throw new RuntimeException("User not found by id " + id);
        }
        System.out.println("UserService -> Found user: " + user);

        return UserMapStruct.MAPPING.mapToFullDto(user);
    }

    public List<UserShortDto> findAll() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication(); //я здесь узнаю кто сейчас заавтосертефицирован
        System.out.println("---> " + auth.getName());

        List<UserEntity> allUsers = userRepository.findAll();
        System.out.println("UserService -> Found all user: " + allUsers);

        return UserMapStruct.MAPPING.mapToListShortDto(allUsers);
    }

    public UserFullDto upDate(UserUpdateDto updateRequest) {
        UserEntity currentUser = securityService.getAuthenticateUser();
        if(!currentUser.getId().equals(updateRequest.getId() )) {  // запрещаю обновлять не саму себя=)
            throw new RuntimeException("Can't update another user");
        }

        UserEntity userForUpdate = userRepository.findById(updateRequest.getId());

        if (userForUpdate == null) {
            throw new RuntimeException("User is not found by id " + userForUpdate.getId());
        }
        if (updateRequest.getId() == null) {
            throw new RuntimeException("UserService -> Can't update entity without id " + updateRequest.getId());
        }
        if (userRepository.findById(updateRequest.getId()) == null) {
            throw new RuntimeException("UserService -> User not found by id: " + updateRequest.getId());
        }

        userForUpdate.setLogin(updateRequest.getLogin());
        userForUpdate.setName(updateRequest.getName());

        UserEntity updatedUser = userRepository.upDate(userForUpdate);
        System.out.println("UserService -> Found for update user: " + updatedUser);

        return UserMapStruct.MAPPING.mapToFullDto(updatedUser);
    }

    public UserFullDto create(UserCreateDto createdRequest) {
        UserEntity user = UserMapStruct.MAPPING.mapToEntity(createdRequest);

        List<UserEntity> existingUsers = userRepository.findAll();
        for (UserEntity existingUser : existingUsers) {
            if (existingUser.getEmail().equals(user.getEmail())) {
                throw new RuntimeException("Email: " + user.getEmail() + "is taken");
            }
        }

        if (user.getId() != null) {
            throw new RuntimeException("UserService -> " +
                    "Can't create entity which already has id " + user.getId());
        }

        UserEntity createdUser = userRepository.create(user);
        System.out.println("UserService -> Created user: " + user);

        return UserMapStruct.MAPPING.mapToFullDto(createdUser);
    }

    public void delete(int id) {
        UserEntity userToDelete = userRepository.findById(id);
        if (userToDelete == null) {
            throw new RuntimeException("User was not found by id: " + id);
        }
        List<CollectionEntity> existingCollection = userToDelete.getCollections();
        List<CommentEntity> existingComments = userToDelete.getComments();
        //забыли фотки удалить (можно удалить дома, в домашке)

        for (CollectionEntity collection : existingCollection) {
            collectionRepository.findById(collection.getId());
        }
        for (CommentEntity comment : existingComments) {
            commentRepository.findById(comment.getId());
        }
        userRepository.delete(id);
    }

}
