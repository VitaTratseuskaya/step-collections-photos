package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoCreateDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoFullDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoShortDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.mapper.PhotoMapStruct;
import by.itstep.collectionsphotos.mapper.PhotoMapper;
import by.itstep.collectionsphotos.repository.interfaces.CollectionRepository;
import by.itstep.collectionsphotos.repository.interfaces.CommentRepository;
import by.itstep.collectionsphotos.repository.interfaces.PhotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhotoService {

    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private CommentRepository commentRepository;


    public PhotoEntity findById(int id) {
        PhotoEntity photo = photoRepository.findById(id);

        if (photo == null) {
            throw new RuntimeException("Photo not found by id " + id);
        }

        System.out.println("PhotoService -> Found photo: " + photo.getId());
        return photo;
    }

    public List<PhotoShortDto> findAll() {
        List<PhotoEntity> allPhoto = photoRepository.findAll();
        System.out.println("PhotoService -> Found all photo: " + allPhoto);

        return PhotoMapStruct.MAPPING.mapToListShortDto(allPhoto);
    }

    public PhotoFullDto upDate(PhotoUpdateDto updateRequest) {
        PhotoEntity photoForUpdate = photoRepository.findById(updateRequest.getId());

        if (photoForUpdate == null) {
            throw new RuntimeException("Photo is not found by id " + photoForUpdate.getId());
        }
        if (updateRequest.getId() == null) {
            throw new RuntimeException("PhotoService -> Can't update entity without id " + updateRequest.getId());
        }
        if (photoRepository.findById(updateRequest.getId()) == null) {
            throw new RuntimeException("PhotoService -> Photo not found by id: " + updateRequest.getId());
        }

        photoForUpdate.setName(updateRequest.getName());

        PhotoEntity updatePhoto = photoRepository.upDate(photoForUpdate);
        System.out.println("PhotoService -> Photo update: " + updatePhoto);

        return PhotoMapStruct.MAPPING.mapToFullDto(updatePhoto);
    }

    public PhotoFullDto create(PhotoCreateDto createdRequest) {
        PhotoEntity photo = PhotoMapStruct.MAPPING.mapToEntity(createdRequest);

        if (photo.getId() != null) {
            throw new RuntimeException("PhotoService -> Can't create entity which already has id " + photo.getId());
        }

        PhotoEntity createdPhoto = photoRepository.create(photo);
        System.out.println("PhotoService -> Created photo: " + createdRequest);

        return PhotoMapStruct.MAPPING.mapToFullDto(createdPhoto);
    }

    public void delete(int id) {
        PhotoEntity photoForDelete = photoRepository.findById(id);
        if (photoForDelete == null) {
            throw new RuntimeException("CollectionService -> Collection not found " + photoForDelete.getId());
        }

        List<CollectionEntity> photoCollection = photoForDelete.getCollections();
        for (CollectionEntity collection : photoCollection) {
            collection.getPhotos().remove(photoForDelete);

            collectionRepository.upDate(collection);
        }
        photoRepository.delete(id);
    }

}
