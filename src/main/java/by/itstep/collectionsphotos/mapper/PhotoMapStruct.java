package by.itstep.collectionsphotos.mapper;

import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoCreateDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoFullDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoShortDto;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface PhotoMapStruct {

    PhotoMapStruct MAPPING = Mappers.getMapper(PhotoMapStruct.class);

    PhotoFullDto mapToFullDto(PhotoEntity photo);

    List<PhotoShortDto> mapToListShortDto(List<PhotoEntity> entities);

    PhotoEntity mapToEntity(PhotoCreateDto dto);
}
