package by.itstep.collectionsphotos.mapper;

import by.itstep.collectionsphotos.dto.commentDTO.CommentCreateDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentFullDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentShortDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface CommentMapStruct {

    CommentMapStruct MAPPER = Mappers.getMapper(CommentMapStruct.class);

    CommentFullDto mapToFullDto(CommentEntity entity);

    List<CommentShortDto> mapToListShortDto(List<CommentEntity> entities);

    CommentEntity mapToEntity(CommentCreateDto dto);
}
