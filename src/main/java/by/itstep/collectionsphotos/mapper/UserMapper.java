package by.itstep.collectionsphotos.mapper;

import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionShortDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserCreateDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserShortDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserFullDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserUpdateDto;
import by.itstep.collectionsphotos.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserMapper {

    @Autowired
    private CollectionMapper collectionMapper;
    @Autowired
    private CommentMapper commentMapper;

   /* public UserFullDto map(UserEntity entity) {
        //1. создать пустое ДТО
        UserFullDto dto = new UserFullDto();
        //2. заполнить
        dto.setId(entity.getId());
        dto.setLogin(entity.getLogin());
        dto.setEmail(entity.getEmail());
        dto.setName(entity.getName());

        List<CollectionShortDto> collectionDtos = collectionMapper.map(entity.getCollections());
        dto.setCollections(collectionDtos);
        // 3. вернуть
        return dto;
    }

    public List<UserShortDto> map(List<UserEntity> entities) {
        List<UserShortDto> dtos = new ArrayList<>();
        for (UserEntity entity : entities) {
            UserShortDto dto = new UserShortDto();
            dto.setId(entity.getId());
            dto.setEmail(entity.getEmail());
            dto.setLogin(entity.getLogin());
            dto.setName(entity.getName());

            dtos.add(dto);
        }
        return dtos;
    }

    public UserEntity map(UserCreateDto dto) {
        UserEntity entity = new UserEntity();
        entity.setLogin(dto.getLogin());
        entity.setEmail(dto.getEmail());
        entity.setPassword(dto.getPassword());
        entity.setName(dto.getName());

        return entity;
    }

    public UserShortDto mapToShort(UserEntity entity) {
        UserShortDto dto = new UserShortDto();

        dto.setId(entity.getId());
        dto.setEmail(entity.getEmail());
        dto.setLogin(entity.getLogin());
        dto.setName(entity.getName());

        return dto;
}
*/
}
