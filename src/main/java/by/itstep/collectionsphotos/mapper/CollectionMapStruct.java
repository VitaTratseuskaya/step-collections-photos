package by.itstep.collectionsphotos.mapper;

import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionCreateDto;
import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionFullDto;
import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionShortDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface CollectionMapStruct {

    CollectionMapStruct MAPPER = Mappers.getMapper(CollectionMapStruct.class);

    CollectionFullDto mapToFullDto(CollectionEntity entity);

    List<CollectionShortDto> mapToListShortDto(List<CollectionEntity> entities);

    CollectionEntity mapToEntity(CollectionCreateDto dto);
}
