package by.itstep.collectionsphotos.mapper;

import by.itstep.collectionsphotos.dto.UserDTO.UserCreateDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserFullDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserShortDto;
import by.itstep.collectionsphotos.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UserMapStruct {

    UserMapStruct MAPPING = Mappers.getMapper(UserMapStruct.class);

    UserFullDto mapToFullDto(UserEntity user);

    List<UserShortDto> mapToListShortDto(List<UserEntity> entities);

    UserEntity mapToEntity(UserCreateDto dto);

}
