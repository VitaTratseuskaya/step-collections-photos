package by.itstep.collectionsphotos.mapper;

import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoCreateDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoFullDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoShortDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoUpdateDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentFullDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentShortDto;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PhotoMapper {

    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private CollectionMapper collectionMapper;

 /*   public PhotoFullDto map(PhotoEntity entity) {
        PhotoFullDto dto = new PhotoFullDto();

        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setRating(entity.getRating());
        dto.setLink(entity.getLink());

        List<CommentShortDto> commentsAll = commentMapper.map(entity.getComments());
        dto.setComments(commentsAll);

        return dto;
    }

    public List<PhotoShortDto> map(List<PhotoEntity> entities) {

        List<PhotoShortDto> dtos = new ArrayList<>();
        for (PhotoEntity entity : entities) {
            PhotoShortDto dto = new PhotoShortDto();
            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setRating(entity.getRating());
            dto.setLink(entity.getLink());

            dtos.add(dto);
        }
        return dtos;
    }

    public PhotoEntity map(PhotoCreateDto dto) {
        PhotoEntity entity = new PhotoEntity();
        entity.setName(dto.getName());
        entity.setLink(dto.getLink());

        return entity;
    }
*/
}
