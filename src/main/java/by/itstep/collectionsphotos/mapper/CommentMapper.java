package by.itstep.collectionsphotos.mapper;

import by.itstep.collectionsphotos.dto.UserDTO.UserCreateDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserFullDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserShortDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentCreateDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentFullDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentShortDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentUpdateDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CommentMapper {

    @Autowired
    private UserMapper userMapper;

  /*  public CommentFullDto map(CommentEntity entity) {
        CommentFullDto dto = new CommentFullDto();
        dto.setId(entity.getId());
        dto.setMessage(entity.getMessage());
        dto.setPhotoId(entity.getPhotos().getId());

        UserShortDto userDto = userMapper.mapToShort(entity.getUser());
        dto.setUser(userDto);

        return dto;
    }

    public List<CommentShortDto> map (List<CommentEntity> entities) {
        List<CommentShortDto> dtos = new ArrayList<>();
        for(CommentEntity entity : entities){
            CommentShortDto dto = new CommentShortDto();
            dto.setId(entity.getId());
            dto.setMessage(entity.getMessage());

            dtos.add(dto);
        }
        return dtos;
    }

    public CommentEntity map(CommentCreateDto dto) {
        CommentEntity entity = new CommentEntity();
        entity.setMessage(dto.getMessage());

        return  entity;
    }*/

}
