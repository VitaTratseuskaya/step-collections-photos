package by.itstep.collectionsphotos.dto.CollectionDTO;

import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoShortDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserShortDto;
import lombok.Data;

import java.util.List;

@Data
public class CollectionShortDto {

    private Integer id;
    private String name;
    private String description;

    private List<PhotoShortDto> photos;
    private UserShortDto user;

}
