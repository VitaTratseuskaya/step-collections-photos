package by.itstep.collectionsphotos.dto.CollectionDTO;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CollectionCreateDto {

    @NotNull
    private Integer userId;

    @NotBlank(message = "Name can not by blank")
    private String name;

    private String description;



}
