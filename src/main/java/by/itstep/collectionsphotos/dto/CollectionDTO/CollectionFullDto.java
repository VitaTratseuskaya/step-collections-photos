package by.itstep.collectionsphotos.dto.CollectionDTO;

import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoShortDto;
import by.itstep.collectionsphotos.dto.UserDTO.UserShortDto;
import lombok.Data;

import java.util.List;


@Data
public class CollectionFullDto {

    private Integer id;

    private String name;
    private String description;

    private UserShortDto user;
    private List<PhotoShortDto> photos;

}
