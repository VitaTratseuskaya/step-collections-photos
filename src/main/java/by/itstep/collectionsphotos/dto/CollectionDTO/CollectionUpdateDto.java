package by.itstep.collectionsphotos.dto.CollectionDTO;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CollectionUpdateDto {

    @NotNull
    private Integer id;

    @NotBlank(message = "Name can not by blank")
    private String name;

    @NotBlank(message = "Description can not by blank")
    private String description;
}
