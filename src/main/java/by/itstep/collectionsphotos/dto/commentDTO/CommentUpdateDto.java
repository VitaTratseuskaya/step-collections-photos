package by.itstep.collectionsphotos.dto.commentDTO;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CommentUpdateDto {

    @NotNull
    private Integer id;

    private String message;

}
