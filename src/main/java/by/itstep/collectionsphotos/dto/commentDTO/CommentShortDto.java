package by.itstep.collectionsphotos.dto.commentDTO;

import by.itstep.collectionsphotos.dto.UserDTO.UserShortDto;
import lombok.Data;

@Data
public class CommentShortDto {

    private Integer Id;

    private String message;
    private Integer photoId;
    private UserShortDto user;
}
