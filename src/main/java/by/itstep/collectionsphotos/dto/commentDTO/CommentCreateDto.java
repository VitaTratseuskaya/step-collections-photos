package by.itstep.collectionsphotos.dto.commentDTO;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CommentCreateDto {

    private String message;

    @NotNull
    private Integer photoId;

    @NotNull
    private Integer userId;

}
