package by.itstep.collectionsphotos.dto.commentDTO;

import by.itstep.collectionsphotos.dto.UserDTO.UserShortDto;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

@Data
public class CommentFullDto {

    private Integer id;
    private String message;
    private Integer photoId;
    private UserShortDto user;

}
