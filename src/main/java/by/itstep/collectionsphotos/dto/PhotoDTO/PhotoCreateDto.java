package by.itstep.collectionsphotos.dto.PhotoDTO;

import by.itstep.collectionsphotos.entity.UserEntity;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class PhotoCreateDto {

    @NotBlank(message = "Link can not by blank")
    private String link;

    @NotBlank(message = "Name can not by blank")
    private String name;

}
