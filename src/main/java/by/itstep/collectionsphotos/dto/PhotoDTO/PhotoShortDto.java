package by.itstep.collectionsphotos.dto.PhotoDTO;

import lombok.Data;

@Data
public class PhotoShortDto {

    private Integer id;
    private String link;
    private String name;
    private Integer rating;

}
