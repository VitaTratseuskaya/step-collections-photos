package by.itstep.collectionsphotos.dto.PhotoDTO;

import by.itstep.collectionsphotos.dto.commentDTO.CommentFullDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentShortDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import lombok.Data;

import java.util.List;

@Data
public class PhotoFullDto {

    private Integer Id;
    private String link;
    private String name;
    private Integer rating;
    private List<CommentShortDto> comments;

}
