package by.itstep.collectionsphotos.dto.PhotoDTO;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class PhotoUpdateDto {

    @NotNull
    private Integer Id;

    @NotBlank(message = "Name can not by blank")
    private String name;


}
