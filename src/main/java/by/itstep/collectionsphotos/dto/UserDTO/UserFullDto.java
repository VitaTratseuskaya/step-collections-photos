package by.itstep.collectionsphotos.dto.UserDTO;

import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionShortDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentShortDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserFullDto {
 //UserFullDto будет отправлен пользователю для отображения профиля
    private Integer id;
    private String login;
    private String email;
    private String name;

    private List<CollectionShortDto> collections = new ArrayList<>();//todo
    private List<CommentShortDto> comments = new ArrayList<>();
}
