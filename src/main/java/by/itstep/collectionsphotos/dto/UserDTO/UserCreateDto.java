package by.itstep.collectionsphotos.dto.UserDTO;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Data
public class UserCreateDto {

    @NotBlank(message = "Login can not by blank")
    @Size(min = 3, max = 20, message = "Login length must be between 3 and 20")
    private String login;

    @NotNull(message = "Email must have invalid format")
    @Email(message = "Email can not be null")
    private String email;

    @NotBlank(message = "Password can not by blank")
    @Size(min = 8, max = 100, message = "Password length must between 8 and 100")
    private String password;

    @NotBlank(message = "Name can not by blank")
    private String name;

}