package by.itstep.collectionsphotos.dto.UserDTO;

import lombok.Data;

@Data
public class UserShortDto {
    //UserShortDto будет отправлен клиенту для отображения списка всех Users

    private Integer id;  //всегда оставляем
    private String login;
    private String email;
    private String name;

}
