package by.itstep.collectionsphotos.dto.UserDTO;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserUpdateDto {

    @NotNull
    private Integer id;

    @NotBlank(message = "Login can not by blank")
    @Size(min = 3, max = 20, message = "Login length must be between 3 and 20")
    private String login;

    @NotBlank(message = "Name can not by blank")
    private String name;
}
