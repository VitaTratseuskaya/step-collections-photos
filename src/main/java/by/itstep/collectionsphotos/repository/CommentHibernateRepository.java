package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.repository.interfaces.CommentRepository;
import by.itstep.collectionsphotos.utils.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class CommentHibernateRepository implements CommentRepository {

    @Override
    public CommentEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        CommentEntity found = em.find(CommentEntity.class, id);

        em.getTransaction().commit();
        em.close();
        return found;
    }

    @Override
    public List<CommentEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        List<CommentEntity> allComment =
                em.createNativeQuery("SELECT * FROM comments", CommentEntity.class).getResultList();

        em.getTransaction().commit();
        em.close();

        return allComment;
    }

    @Override
    public CommentEntity create(CommentEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(entity); //сохранить

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public CommentEntity upDate(CommentEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.merge(entity);

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public void delete(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        CommentEntity deleteComment = em.find(CommentEntity.class, id);
        em.remove(deleteComment);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM comments").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
