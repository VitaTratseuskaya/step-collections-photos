package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import by.itstep.collectionsphotos.utils.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class UserHibernateRepository implements UserRepository {

    @Override
    public UserEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        UserEntity foundUser = em.find(UserEntity.class, id);

        if(foundUser != null) {
            Hibernate.initialize(foundUser.getCollections());// загрузит и коллецкции тоже
            Hibernate.initialize(foundUser.getComments());
        }
        em.getTransaction().commit();
        em.close();
        return foundUser;
    }

    @Override
    public List<UserEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        List<UserEntity> allUser =
                em.createNativeQuery("SELECT * FROM users", UserEntity.class).getResultList();

        //Todo: remove me
        for(UserEntity user : allUser) {
            Hibernate.initialize(user.getCollections());
            Hibernate.initialize(user.getComments());
        }

        em.getTransaction().commit();
        em.close();

        return allUser;
    }

    @Override
    public UserEntity create(UserEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(entity); //сохранить

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public UserEntity upDate(UserEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        UserEntity entityUpDate = em.find(UserEntity.class, entity.getId());
        entityUpDate.setName(entity.getName());
        entityUpDate.setLogin(entity.getLogin());

        Hibernate.initialize(entityUpDate.getCollections());
        Hibernate.initialize(entityUpDate.getComments());

        em.getTransaction().commit();
        em.close();

        return entityUpDate;
    }

    @Override
    public void delete(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        UserEntity deleteUser = em.find(UserEntity.class, id);
        em.remove(deleteUser);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM users").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public UserEntity findByUserlogin(String userlogin) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        String sql = String.format("SELECT * FROM users WHERE login = '%s';", userlogin);
        UserEntity foundUser = (UserEntity) em.createNativeQuery(sql, UserEntity.class).getSingleResult();

        em.getTransaction().commit();
        em.close();

        return foundUser;
    }
}
