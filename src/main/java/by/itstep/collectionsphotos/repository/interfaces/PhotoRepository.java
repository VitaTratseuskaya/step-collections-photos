package by.itstep.collectionsphotos.repository.interfaces;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;

import java.util.List;

public interface PhotoRepository {

    PhotoEntity findById(int id);  //<- public

    List<PhotoEntity> findAll();

    PhotoEntity create(PhotoEntity entity);

    PhotoEntity upDate(PhotoEntity entity);

    void delete(int id);

    void deleteAll();

}


