package by.itstep.collectionsphotos.repository.interfaces;

import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;

import java.util.List;

public interface CommentRepository {

    CommentEntity findById(int id);  //<- public

    List<CommentEntity> findAll();

    CommentEntity create(CommentEntity entity);

    CommentEntity upDate(CommentEntity entity);

    void delete(int id);

    void deleteAll();

}


