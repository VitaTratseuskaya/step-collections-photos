package by.itstep.collectionsphotos.repository.interfaces;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.UserEntity;

import java.util.Collection;
import java.util.List;

public interface CollectionRepository {

    CollectionEntity findById(int id);  //<- public

    List<CollectionEntity> findAll();

    CollectionEntity create(CollectionEntity entity);

    CollectionEntity upDate(CollectionEntity entity);

    void delete(int id);

    void deleteAll();
}


