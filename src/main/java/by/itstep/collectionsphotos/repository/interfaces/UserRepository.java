package by.itstep.collectionsphotos.repository.interfaces;

import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.utils.EntityManagerUtils;

import java.util.List;

public interface UserRepository {

    UserEntity findById(int id);  //<- public

    List<UserEntity> findAll();

    UserEntity create(UserEntity entity);

    UserEntity upDate(UserEntity entity);

    void delete(int id);

    void deleteAll();

    UserEntity findByUserlogin(String userlogin);
}
