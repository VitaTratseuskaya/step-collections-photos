package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.repository.interfaces.CollectionRepository;
import by.itstep.collectionsphotos.utils.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class CollectionHibernateRepository implements CollectionRepository {

    @Override
    public CollectionEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        CollectionEntity foundCollection = em.find(CollectionEntity.class, id);
        PhotoEntity found = em.find(PhotoEntity.class, id);

        em.getTransaction().commit();
        em.close();
        return foundCollection;
    }

    @Override
    public List<CollectionEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List<CollectionEntity> allCollection =
                em.createNativeQuery("SELECT * FROM collections", CollectionEntity.class).getResultList();

        em.getTransaction().commit();
        em.close();

        return allCollection;
    }

    @Override
    public CollectionEntity create(CollectionEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(entity); //сохранить

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public CollectionEntity upDate(CollectionEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.merge(entity);

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public void delete(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        CollectionEntity deleteUser = em.find(CollectionEntity.class, id);
        em.remove(deleteUser);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM collections").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
