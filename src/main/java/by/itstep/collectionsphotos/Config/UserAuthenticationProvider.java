package by.itstep.collectionsphotos.Config;

import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class UserAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String userLogin = authentication.getName();
        String password = authentication.getCredentials().toString();

        System.out.println("username " + userLogin);
        System.out.println("password " + password);

        UserEntity foundUser = userRepository.findByUserlogin(userLogin);


        if (foundUser != null && foundUser.getPassword().equals(password)) {
            return new UsernamePasswordAuthenticationToken(userLogin, password, new ArrayList<>());
        } else {
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }
}
