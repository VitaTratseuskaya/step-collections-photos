package by.itstep.collectionsphotos.Config;

import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SecurityService {

    @Autowired
    private UserRepository userRepository;

    public UserEntity getAuthenticateUser() {

        if(getAuthenticateUser() == null) {
            return null;
        }

        String login = SecurityContextHolder
            .getContext()
            .getAuthentication()
            .getName();

        return userRepository.findByUserlogin(login);
    }
}
