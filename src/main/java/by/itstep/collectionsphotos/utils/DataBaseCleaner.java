package by.itstep.collectionsphotos.utils;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.repository.interfaces.CollectionRepository;
import by.itstep.collectionsphotos.repository.interfaces.CommentRepository;
import by.itstep.collectionsphotos.repository.interfaces.PhotoRepository;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class DataBaseCleaner {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CollectionRepository collectionRepository;


    public void clean() {
        for (CollectionEntity collection : collectionRepository.findAll()) {
            collection.setPhotos(new ArrayList<>());
            collectionRepository.upDate(collection);
        }

        collectionRepository.deleteAll();
        commentRepository.deleteAll();
        photoRepository.deleteAll();
        userRepository.deleteAll();
    }
}
