package by.itstep.collectionsphotos.controller;

import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoCreateDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoFullDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoShortDto;
import by.itstep.collectionsphotos.dto.PhotoDTO.PhotoUpdateDto;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class PhotoController {

    @Autowired
    private PhotoService photoService;

    @ResponseBody
    @RequestMapping(value = "/photos", method = RequestMethod.POST)
    public PhotoFullDto create(@Valid @RequestBody PhotoCreateDto createRequest) {
        PhotoFullDto created = photoService.create(createRequest);
        return created;
    }

    @ResponseBody
    @RequestMapping(value = "/photos", method = RequestMethod.GET)
    public List<PhotoShortDto> findAll() {
        List<PhotoShortDto> allPhotos = photoService.findAll();
        return allPhotos;
    }

    @ResponseBody
    @RequestMapping(value = "/photos/{id}", method = RequestMethod.GET)
    public PhotoEntity findById(@PathVariable int id) {
        PhotoEntity photo = photoService.findById(id);
        return photo;
    }

    @ResponseBody
    @RequestMapping(value = "/photos", method = RequestMethod.PUT)
    public PhotoFullDto upDate(@Valid @RequestBody PhotoUpdateDto updateRequest) {
        PhotoFullDto updatedPhotos = photoService.upDate(updateRequest);
        return updatedPhotos;
    }

    @ResponseBody
    @RequestMapping(value = "/photos/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id) {
        photoService.delete(id);
    }
}
/*/*

 1. Заменить существующие Mapper'ы используя Mapstruct
    - https://mapstruct.org/
    - https://www.baeldung.com/mapstruct

 2. Добавить Flyway миграции
    - https://www.baeldung.com/database-migrations-with-flyway

 3. Добавить SpringSecurity BasicAuth
    - https://spring.io/guides/gs/securing-web/ (Раздел Set up Spring Security, Блок с html можно пропустить)

 */