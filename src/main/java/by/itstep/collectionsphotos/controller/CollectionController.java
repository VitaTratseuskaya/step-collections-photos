package by.itstep.collectionsphotos.controller;

import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionCreateDto;
import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionFullDto;
import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionShortDto;
import by.itstep.collectionsphotos.dto.CollectionDTO.CollectionUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.repository.interfaces.UserRepository;
import by.itstep.collectionsphotos.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CollectionController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CollectionService collectionService;

    @ResponseBody
    @RequestMapping(value = "/users/{userId}/collections", method = RequestMethod.POST)
    public CollectionFullDto create(@Valid @RequestBody CollectionCreateDto createRequest) {
        CollectionFullDto createdCollection = collectionService.create(createRequest);
        return createdCollection;
    }

    @ResponseBody
    @RequestMapping(value = "/collections/{id}", method = RequestMethod.GET)
    public CollectionFullDto findById(@PathVariable int id) {
        CollectionFullDto collection = collectionService.findById(id);
        return collection;
    }

    @ResponseBody
    @RequestMapping(value = "/collections", method = RequestMethod.GET)
    public List<CollectionShortDto> findAll() {
        List<CollectionShortDto> allCollection = collectionService.findAll();
        return allCollection;
    }

    @ResponseBody
    @RequestMapping(value = "/users/{userId}/collections", method = RequestMethod.PUT)
    public CollectionFullDto upDate (@Valid @RequestBody CollectionUpdateDto updateRequest) {
        CollectionFullDto updatedUser = collectionService.upDate(updateRequest);
        return updatedUser;
    }

    @ResponseBody
    @RequestMapping(value = "/collections/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id) {
        collectionService.delete(id);
    }
}


