package by.itstep.collectionsphotos.controller;

import by.itstep.collectionsphotos.dto.commentDTO.CommentCreateDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentFullDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentShortDto;
import by.itstep.collectionsphotos.dto.commentDTO.CommentUpdateDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    @ResponseBody
    @RequestMapping(value = "/users/{userId}/photos/{photoId}/comments", method = RequestMethod.POST)
    public CommentFullDto create(@Valid @RequestBody CommentCreateDto createRequest) {
        CommentFullDto createdComment = commentService.create(createRequest);
        return createdComment;
    }

    @ResponseBody
    @RequestMapping(value = "/comments/{id}", method = RequestMethod.GET)
    public CommentFullDto findById(@PathVariable int id) {
        CommentFullDto comment = commentService.findById(id);
        return comment;
    }

    @ResponseBody
    @RequestMapping(value = "/comments", method = RequestMethod.GET)
    public List<CommentShortDto> findAll() {
        List<CommentShortDto> allComments = commentService.findAll();
        return allComments;
    }

    @ResponseBody
    @RequestMapping(value = "/users/{userId}/photos/{photoId}/comments", method = RequestMethod.PUT)
    public CommentFullDto upDate (@Valid @RequestBody CommentUpdateDto updateRequest) {
        CommentFullDto updatedComment = commentService.upDate(updateRequest);
        return updatedComment;
    }

    @ResponseBody
    @RequestMapping(value = "/comments/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id) {
        commentService.delete(id);
    }
}
