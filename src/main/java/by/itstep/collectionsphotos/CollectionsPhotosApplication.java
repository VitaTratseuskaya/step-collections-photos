package by.itstep.collectionsphotos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CollectionsPhotosApplication {

    public static void main(String[] args) {
        SpringApplication.run(CollectionsPhotosApplication.class, args);

    }
}
